//
//  main.m
//  Lab2_Part 1 & 2
//
//  Created by Thomas Matthias on 1/28/16.
//  Copyright © 2016 PurpleSun. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#define SAMPLE_RATE 44100
#define DURATION 5.0
#define FILENAME_FORMAT @"%0.3f-square.aif"

//------------------------------------------------------------
//Lab 2 Part 1
//------------------------------------------------------------

//Generates Filename
NSString* genFileName(NSString* FileNameFormat, double in_hz){
    NSString *filename = [NSString stringWithFormat: FileNameFormat, in_hz];
    return filename;
}

//Generates Filepath
NSString* genFilePath(NSString* input_fileName){
    NSString *filePath = [[[NSFileManager defaultManager]
                           currentDirectoryPath]stringByAppendingPathComponent: input_fileName];
    return filePath;
}

//Generates FileURL
NSURL* genFileURL(NSString* input_filePath){
    NSURL *fileURL= [NSURL fileURLWithPath: input_filePath];
    return fileURL;
}

AudioStreamBasicDescription genASBD(int inSampleRate){
    AudioStreamBasicDescription asbd;
    memset(&asbd, 0, sizeof(asbd));
    asbd.mSampleRate = inSampleRate;
    asbd.mFormatID = kAudioFormatLinearPCM;
    asbd.mFormatFlags = kAudioFormatFlagIsBigEndian | kAudioFormatFlagIsSignedInteger |kAudioFormatFlagIsPacked;
    asbd.mBitsPerChannel = 16;
    asbd.mChannelsPerFrame = 1;
    asbd.mFramesPerPacket = 1;
    asbd.mBytesPerFrame = 2;
    asbd.mBytesPerPacket = 2;
    return asbd;
}


void genWaveFile (int input_hz) {
        //Convert and confirm hz value
        double hz = input_hz;
        assert (hz > 0);
        
        // Set up the file
        AudioFileID audioFile;
        OSStatus audioErr = noErr;
        
        NSString* currentFileName = genFileName(FILENAME_FORMAT, hz);
        NSString* currentFilePath = genFilePath(currentFileName);
        NSURL* currentFileURL = genFileURL(currentFilePath);
        AudioStreamBasicDescription asbd = genASBD(SAMPLE_RATE);
        
        
        audioErr = AudioFileCreateWithURL((__bridge CFURLRef)currentFileURL, kAudioFileAIFFType, &asbd, kAudioFileFlags_EraseFile, &audioFile);
        assert (audioErr == noErr);
        
        // Start writing samples
        long maxSampleCount = SAMPLE_RATE;
        long sampleCount = 0;
        UInt32 bytesToWrite = 2;
        double wavelengthInSamples = SAMPLE_RATE / hz;
        while (sampleCount < maxSampleCount) {
            for (int i=0; i < wavelengthInSamples; i++) {
                SInt16 sample;
                if (i < wavelengthInSamples / 2) {
                    sample = CFSwapInt16HostToBig (SHRT_MAX);
                }
                else {
                    sample = CFSwapInt16HostToBig (SHRT_MIN);
                }
                audioErr = AudioFileWriteBytes(audioFile, false, sampleCount*2, &bytesToWrite, &sample);
                assert (audioErr == noErr);
                sampleCount++;
            }
        }
        
        audioErr = AudioFileClose(audioFile);
        assert (audioErr == noErr);
        NSLog (@"wrote %ld samples", maxSampleCount);
    }

int main(int argc, const char * argv[]) {
    
    //Part 1 Execution, Code Above
    genWaveFile(440);
    
    //Part 2 Execution, Code in Lab2_Part2 Folder
    return NSApplicationMain(argc, argv);
}
