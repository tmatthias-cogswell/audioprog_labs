#import <Cocoa/Cocoa.h>

@interface AppController : NSObject
{
    IBOutlet NSTableView *myTable;
    NSMutableArray	*deviceArray;
    AudioObjectID	*devices;
    AudioObjectPropertyListenerBlock AOPropertyListenerBlock;
}
- (void)updateDeviceList;
@end